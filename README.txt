 CONTENTS OF THIS FILE
 ---------------------
  * Slovenský jazyk
   - Súhrn
   - Inštalácia           
  * English language
   - Summary
   - Installation  

 SLOVENSKÝ JAZYK
 ---------------
 
 -- SÚHRN --
Slovenský Drupal inštalačný profil slúži na rýchlejšie a ľahšie nainštalovanie
Drupalu v Slovenskom jazyku. Inštaluje rovnaké moduly, ako štandardnáinštalácia
v základnej inštalácii Drupalu s ďalšími dvoma modulmi, ktoré slúžia na import
a používanie prekladu.

 -- INŠTALÁCIA --
Podrobné návody čítajte v súboroch, ktoré sú v koreňovom adresári Drupal
inštalácie:
  * README.txt
  * INSTALL.txt
  * INSTALL.[typdatabazy].txt

 ENGLISH LANGUAGE
 ----------------
 
 -- SUMMARY --
Slovak Drupal Installation Profile is intended for faster and easier
installation of Drupal in Slovak language. It will install the same modules
as standard installation in Drupal core  with two other modules intended for
import and use of translation.

 -- INSTALLATION --
For detailed instructions read the files that are in the root directory
of Drupal installation:
  * README.txt
  * INSTALL.txt
  * INSTALL.[databasetype].txt

---------- END ------------
