<?php
/**
 * @file
 * Enables modules and site configuration for a slovak_install installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function slovak_install_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
  $form['server_settings']['site_default_country']['#default_value'] = 'SK';
  // Remove attribute class "timezone-detect" for correct change #default_value
  // and add correct #attribute name with comma-separated.
  $form['server_settings']['date_default_timezone']['#attributes'] = array('name' => 'date-default-timezone');
  $form['server_settings']['date_default_timezone']['#default_value'] = 'Europe/Bratislava';
}

/**
 * Implements hook_install_tasks_alter().
 */
function slovak_install_install_tasks_alter(&$tasks, $install_state) {
  // Since we only The Slovak language, define a callback to set this.
  $tasks['install_select_locale']['function'] = 'slovak_install_locale_selection';
}

/**
 * Task handler to set the language to Slovak.
 */
function slovak_install_locale_selection(&$install_state) {
  // Find available locales.
  $profilename = $install_state['parameters']['profile'];
  $locales = install_find_locales($profilename);
  $install_state['locales'] += $locales;
  // Install only the Slovak language.
  $install_state['parameters']['locale'] = 'sk';
}

/**
 * Implements hook_install_tasks().
 */
function slovak_install_install_tasks() {
  // Determine whether translation import tasks will need to be performed.
  $needs_translations = $install_state['parameters']['locale'] = 'sk';

  return array(
    'slovak_install_import_translation' => array(
      'display_name' => st('Set up translations'),
      'display' => $needs_translations,
      'run' => $needs_translations ? INSTALL_TASK_RUN_IF_NOT_COMPLETED : INSTALL_TASK_SKIP,
      'type' => 'batch',
    ),
  );
}

/**
 * Installation step callback.
 */
function slovak_install_import_translation() {

  // Build batch with l10n_update module.
  $history = l10n_update_get_history();
  module_load_include('check.inc', 'l10n_update');
  $available = l10n_update_available_releases();
  $updates = l10n_update_build_updates($history, $available);

  module_load_include('batch.inc', 'l10n_update');
  $updates = _l10n_update_prepare_updates($updates, NULL, array());
  $batch = l10n_update_batch_multiple($updates, LOCALE_IMPORT_KEEP);
  return $batch;
}
